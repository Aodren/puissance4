/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puissance4.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/27 14:52:34 by abary             #+#    #+#             */
/*   Updated: 2016/02/28 10:23:17 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"
#include "libft.h"

static void	ft_display_player(int player)
{
	if (player)
		ft_putendl("Player's turn");
	else
		ft_putendl("Computer's turn");
}

static int	*ft_init_val(int winner, int *player, long *size, int *tabline)
{
	if (!(tabline = (int *)ft_memalloc(sizeof(int) * g_line)))
		return (0);
	*size = g_line * g_col;
	*size = *size % 2 ? *size - 1 : *size;
	winner = 0;
	*player = ft_select_player();
	return (tabline);
}

int			ft_puissance4(char **puissance4)
{
	int		input;
	int		*tabline;
	long	size;
	int		tab[3];

	tabline = NULL;
	if (!(tabline = ft_init_val(tab[0], &tab[1], &size, tabline)))
		return (-10);
	tab[PIECE] = 0;
	while ((tab[WINNER] = ft_check_winner(puissance4, size, tab[PIECE])) == 0)
	{
		ft_display_player(tab[PLAYER]);
		ft_display_board(puissance4);
		if (tab[PLAYER])
			input = ft_recup_input(tabline);
		else
			input = ft_ia(puissance4, tab[PIECE]);
		ft_pos_piece(puissance4, input - 1, tab[PIECE]);
		tab[PLAYER] = tab[PLAYER] ? 0 : 1;
		tab[PIECE] = tab[PIECE] ? 0 : 1;
		--size;
	}
	ft_display_victory(puissance4, tab[PLAYER], tab[WINNER]);
	return (tab[WINNER]);
}
