/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/27 16:16:49 by abary             #+#    #+#             */
/*   Updated: 2016/02/28 10:10:48 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"

int		ft_check_column(int *tabline, int col)
{
	if (tabline[col] == g_line)
		return (0);
	tabline[col]++;
	return (1);
}

void	ft_uncheck_column(int *tabline, int col)
{
	tabline[col]--;
}

int		ft_check_column_recu(char **tab, int col)
{
	int i;

	i = 0;
	while (i < g_line)
	{
		if (tab[i][col] == ' ')
			return (1);
		++i;
	}
	return (0);
}
