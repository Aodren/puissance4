/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ia_norme.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/28 11:11:06 by abary             #+#    #+#             */
/*   Updated: 2016/02/28 11:16:17 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "puissance4.h"

void	ft_destruct_tab(int *tab)
{
	if (tab)
		free(tab);
}

void	ft_val_max_min(int *tab, int i)
{
	if (tab[VALMAX] < tab[RETMAX])
	{
		tab[COLMAX] = i;
		tab[VALMAX] = tab[RETMAX];
	}
	if (tab[VALMIN] < tab[RETMIN])
	{
		tab[COLMIN] = i;
		tab[VALMIN] = tab[RETMIN];
	}
}
