/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_max.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/28 10:06:43 by abary             #+#    #+#             */
/*   Updated: 2016/02/28 11:30:04 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"

int		ft_check_max(char **p4, int piece, int col, int ligne)
{
	int		poid;
	char	p;

	p = piece ? 'O' : 'X';
	poid = ft_check_line_vd(p4, ligne, col, p) * 100;
	poid += ft_check_col_vd(p4, ligne, col, p) * 50;
	poid += ft_check_diag_vd_gauche(p4, ligne, col) * 150;
	poid += ft_check_diag_vd_droite(p4, ligne, col) * 150;
	if (col == 0 || col == g_col)
		poid -= 1000;
	else if (col + 1 == 1 || col == g_col - 1)
		poid -= 500;
	else if (g_col / 2 == col)
		poid += 1000;
	else if (g_col / 2 + 1 == col)
		poid += 500;
	else if (g_col / 2 - 1 == col)
		poid += 500;
	if (ligne == 0)
		poid -= 1000;
	if (ligne == 1)
		poid -= 500;
	return (poid);
}
