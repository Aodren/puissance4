/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/27 21:40:48 by abary             #+#    #+#             */
/*   Updated: 2016/02/28 11:18:55 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "puissance4.h"

#include "libft.h"

void		ft_calcul_ai(char **p4, int piece, int *tab, int pro)
{
	int col;
	int nbr;
	int ligne;

	col = 0;
	if (pro > 0)
	{
		while (col < g_col)
		{
			if (ft_check_column_recu(p4, col))
			{
				ligne = ft_pos_piece(p4, col, piece);
				nbr = ft_check_max(p4, piece, col, ligne);
				if (tab[RETMAX] < nbr)
					tab[RETMAX] = nbr;
				if (piece)
					ft_calcul_player(p4, 0, tab, --pro);
				else
					ft_calcul_player(p4, 1, tab, --pro);
				ft_ret_piece(p4, col, piece);
			}
			++col;
		}
	}
}

void		ft_calcul_player(char **p4, int piece, int *tab, int pro)
{
	int nbr;
	int ligne;
	int col;

	col = 0;
	if (pro > 0)
	{
		while (col < g_col)
		{
			if (ft_check_column_recu(p4, col))
			{
				ligne = ft_pos_piece(p4, col, piece);
				nbr = ft_check_max(p4, piece, col, ligne);
				if (tab[RETMIN] < nbr)
					tab[RETMIN] = nbr;
				if (piece)
					ft_calcul_ai(p4, 0, tab, --pro);
				else
					ft_calcul_ai(p4, 1, tab, --pro);
				ft_ret_piece(p4, col, piece);
			}
			++col;
		}
	}
}

static int	*ft_init_tab(int *tab)
{
	if (!(tab = ft_memalloc(sizeof(int) * 6)))
		return (NULL);
	tab[VALMAX] = 0;
	tab[VALMIN] = 0;
	tab[COLMAX] = 0;
	tab[COLMIN] = 0;
	tab[RETMIN] = 0;
	tab[RETMAX] = 0;
	return (tab);
}

int			ft_ia(char **puissance4, int piece)
{
	int i;
	int *tab;
	int profondeur;

	tab = NULL;
	tab = ft_init_tab(tab);
	i = ft_win_loose(puissance4, piece);
	if (i > 0)
		return (i);
	profondeur = 10;
	i = -1;
	piece = piece ? 0 : 1;
	while (++i < g_col)
	{
		if (ft_check_column_recu(puissance4, i))
		{
			ft_pos_piece(puissance4, i, piece);
			tab[RETMIN] = 0;
			tab[RETMAX] = 0;
			ft_calcul_player(puissance4, piece, tab, profondeur);
			ft_val_max_min(tab, i);
			ft_ret_piece(puissance4, i, piece);
		}
	}
	return (tab[COLMIN] > tab[COLMAX] ? tab[COLMIN] + 1 : tab[COLMAX] + 1);
}
